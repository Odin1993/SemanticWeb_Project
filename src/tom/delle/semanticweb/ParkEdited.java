package tom.delle.semanticweb;

public class ParkEdited {

	private String name,id ;
	private Number latitude, longitude;
	
	
	public ParkEdited(String id,String name, Number number, Number number2){
		this.id = id;
		this.name = name;
		this.latitude = number;
		this.longitude = number2;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Number getLatitude() {
		return latitude;
	}


	public void setLatitude(Number latitude) {
		this.latitude = latitude;
	}


	public Number getLongitude() {
		return longitude;
	}


	public void setLongitude(Number longitude) {
		this.longitude = longitude;
	}
	
	
	
	
	
	
}
