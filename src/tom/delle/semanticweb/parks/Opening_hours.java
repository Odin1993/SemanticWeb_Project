
package tom.delle.semanticweb.parks;

import java.util.List;

public class Opening_hours{
   	private boolean open_now;
   	private List weekday_text;

 	public boolean getOpen_now(){
		return this.open_now;
	}
	public void setOpen_now(boolean open_now){
		this.open_now = open_now;
	}
 	public List getWeekday_text(){
		return this.weekday_text;
	}
	public void setWeekday_text(List weekday_text){
		this.weekday_text = weekday_text;
	}
}
