
package tom.delle.semanticweb.parks;

import java.util.List;

public class Photos{
   	private Number height;
   	private List html_attributions;
   	private String photo_reference;
   	private Number width;

 	public Number getHeight(){
		return this.height;
	}
	public void setHeight(Number height){
		this.height = height;
	}
 	public List getHtml_attributions(){
		return this.html_attributions;
	}
	public void setHtml_attributions(List html_attributions){
		this.html_attributions = html_attributions;
	}
 	public String getPhoto_reference(){
		return this.photo_reference;
	}
	public void setPhoto_reference(String photo_reference){
		this.photo_reference = photo_reference;
	}
 	public Number getWidth(){
		return this.width;
	}
	public void setWidth(Number width){
		this.width = width;
	}
}
