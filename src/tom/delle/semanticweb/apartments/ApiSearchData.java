
package tom.delle.semanticweb.apartments;


public class ApiSearchData{
   	private String searchField1;
   	private String searchField2;
   	private String searchField3;

 	public String getSearchField1(){
		return this.searchField1;
	}
	public void setSearchField1(String searchField1){
		this.searchField1 = searchField1;
	}
 	public String getSearchField2(){
		return this.searchField2;
	}
	public void setSearchField2(String searchField2){
		this.searchField2 = searchField2;
	}
 	public String getSearchField3(){
		return this.searchField3;
	}
	public void setSearchField3(String searchField3){
		this.searchField3 = searchField3;
	}
}
