
package tom.delle.semanticweb.apartments;


import com.google.gson.annotations.SerializedName;

public class Apartment{
   	
	
	@SerializedName("@creation")
	private String creation;
	@SerializedName("@id")
	private String id;
	@SerializedName("@modification")
   	private String modification;
	@SerializedName("@publishDate")
	private String publishDate;
	@SerializedName("@xlink.href")
   	private String xlink_href;
   	private Number realEstateId;
   	@SerializedName("resultlist.realEstate")
   	private Resultlist_realEstate resultlist_realEstate;

 	public String getcreation(){
		return this.creation;
	}
	public void setcreation(String creation){
		this.creation = creation;
	}
 	public String getid(){
		return this.id;
	}
	public void setid(String id){
		this.id = id;
	}
 	public String getmodification(){
		return this.modification;
	}
	public void setmodification(String modification){
		this.modification = modification;
	}
 	public String getpublishDate(){
		return this.publishDate;
	}
	public void setpublishDate(String publishDate){
		this.publishDate = publishDate;
	}
 	public String getxlink_href(){
		return this.xlink_href;
	}
	public void setxlink_href(String xlink_href){
		this.xlink_href = xlink_href;
	}
 	public Number getRealEstateId(){
		return this.realEstateId;
	}
	public void setRealEstateId(Number realEstateId){
		this.realEstateId = realEstateId;
	}
 	public Resultlist_realEstate getResultlist_realEstate(){
		return this.resultlist_realEstate;
	}
	public void setResultlist_realEstate(Resultlist_realEstate resultlist_realEstate){
		this.resultlist_realEstate = resultlist_realEstate;
	}
}
