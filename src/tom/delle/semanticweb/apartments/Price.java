
package tom.delle.semanticweb.apartments;


public class Price{
   	private String currency;
   	private String marketingType;
   	private String priceIntervalType;
   	private Number value;

 	public String getCurrency(){
		return this.currency;
	}
	public void setCurrency(String currency){
		this.currency = currency;
	}
 	public String getMarketingType(){
		return this.marketingType;
	}
	public void setMarketingType(String marketingType){
		this.marketingType = marketingType;
	}
 	public String getPriceIntervalType(){
		return this.priceIntervalType;
	}
	public void setPriceIntervalType(String priceIntervalType){
		this.priceIntervalType = priceIntervalType;
	}
 	public Number getValue(){
		return this.value;
	}
	public void setValue(Number value){
		this.value = value;
	}
}
