package tom.delle.semanticweb;

public class DistanceObject {

	private String id,value,endPoint, apartment;
	public DistanceObject(String id, String value, String endPoint, String apartment){
		this.id = id;
		this.value = value;
		this.endPoint = endPoint;
		this.apartment = apartment;
		
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getEndPoint() {
		return endPoint;
	}
	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}
	public String getApartment() {
		return apartment;
	}
	public void setApartment(String apartment) {
		this.apartment = apartment;
	}
	
	public void printOut(){
		System.out.println("");
		System.out.println(id);
		System.out.println(value);
		System.out.println(apartment);
		System.out.println(endPoint);
		System.out.println("");
	}
	
}
