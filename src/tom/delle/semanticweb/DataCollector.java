package tom.delle.semanticweb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.ws.handler.MessageContext.Scope;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import tom.delle.semanticweb.apartments.Apartment;
import tom.delle.semanticweb.parks.Park;
import tom.delle.semanticweb.schools.School;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class DataCollector {

	private HttpURLConnection request;
	private JsonParser jp;
	private JsonElement root;
	private  URL url;
	private Gson gson;
	private String pattern = "\\d{1}\\.\\d+\\,\\d+";
	private Pattern p;
	private String averagePrice;
	
	private Apartment[] apartments;
	private School[] schools;
	private Park[] parks;
	private ArrayList<ApartmentEdited> apartmentsEdited;
	private ArrayList<SchoolEdited> schoolsEdited;
	private ArrayList<ParkEdited> parksEdited;
	
	
	public DataCollector() throws IOException{
		this.gson = new Gson();
		this.jp = new JsonParser();
		
	
	}
	
	public void getSchools(String schoolUrl) throws IOException{
		url = new URL(schoolUrl);
		request = (HttpURLConnection) url.openConnection();
		root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
		this.schools = gson.fromJson(root, School[].class);
		System.out.println(this.schools.length+"originalSchools");
	}
	
	
	public void getApartments(String apartmentUrl) throws IOException{
		url = new URL(apartmentUrl);
		request = (HttpURLConnection) url.openConnection();
		root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
		this.apartments = gson.fromJson(root, Apartment[].class);

		
	}
	
	
	public void getParks(String parkUrl) throws IOException{
		parksEdited = new ArrayList<ParkEdited>();
		url = new URL(parkUrl);
		request = (HttpURLConnection) url.openConnection();
		root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
		this.parks = gson.fromJson(root, Park[].class);
		
	
		
		
	}
	
	
	
	public void setApartmentsEdited(ArrayList<ApartmentEdited> apartmentsEdited) {
		this.apartmentsEdited = apartmentsEdited;
	}

	public void setSchoolsEdited(ArrayList<SchoolEdited> schoolsEdited) {
		this.schoolsEdited = schoolsEdited;
	}

	public void setParksEdited(ArrayList<ParkEdited> parksEdited) {
		this.parksEdited = parksEdited;
	}

	public void getAveragePrice(String priceUrl) throws IOException{
		p = Pattern.compile(pattern);
		
		Document doc = Jsoup.connect(priceUrl).get();
		Elements content = doc.getElementsByClass("rent_index_top_text");
		String htmlString = content.get(0).text();
		Matcher m = p.matcher(htmlString);
		if (m.find()) {
			this.averagePrice=m.group(0);
			String[] array = this.averagePrice.split("\\.");
			this.averagePrice = array[0]+array[1];
			array = this.averagePrice.split(",");
			this.averagePrice = array[0]+"."+array[1];
		
			
			
			
		}
	}
	
	public ArrayList<ApartmentEdited> getApartmentsEdited(){
		
		apartmentsEdited = new ArrayList<ApartmentEdited>();
		for(int i = 0; i<apartments.length;i++){
			
			if(apartments[i].getResultlist_realEstate().getAddress().getWgs84Coordinate()==null)continue;
			
			
			String price =	apartments[i].getResultlist_realEstate().getPrice().getValue().toString();
			String size =	apartments[i].getResultlist_realEstate().getLivingSpace().toString();
			String lat =	apartments[i].getResultlist_realEstate().getAddress().getWgs84Coordinate().getLatitude().toString();
    		String lon =	apartments[i].getResultlist_realEstate().getAddress().getWgs84Coordinate().getLongitude().toString();
			String pricePerSqrt = String.valueOf((Float.parseFloat(price) / Float.parseFloat(size)));
			String belowAverage;
			if(Float.parseFloat(pricePerSqrt)<Float.parseFloat(averagePrice))belowAverage="true";
			else belowAverage = "false";
			String link = apartments[i].getxlink_href().toString();
			String id = link.split("\\/")[link.split("\\/").length-1];
			
			
			ArrayList<DistanceObject> schoolList = new ArrayList<DistanceObject>();
			
			for(int k = 0; k<schoolsEdited.size();k++){
				
				SchoolEdited school = schoolsEdited.get(k);
				String schoolID, distanceID, apartmentID;
				Float lat1, lng1, lat2, lng2, distance;
				
				apartmentID = id;
				schoolID = school.getName();
				distanceID = school.getId();
				lat1 = Float.valueOf(lat);
				lng1 = Float.valueOf(lon);
				lat2 = Float.valueOf(school.getLatitude());
				lng2 = Float.valueOf(school.getLongitude());
				distance = distFrom(lat1, lng1, lat2, lng2);
				if(distance<1000)
				schoolList.add(new DistanceObject(distanceID, String.valueOf(distance), schoolID, apartmentID));
	

				
			}
			
			
			ArrayList<DistanceObject> parkList = new ArrayList<DistanceObject>();
			
			for(int k = 0; k<parksEdited.size();k++){
				
				ParkEdited park = parksEdited.get(k);
				String parkID, distanceID, apartmentID;
				Float lat1, lng1, lat2, lng2, distance;
				
				apartmentID = id;
				parkID = park.getName();
				distanceID = park.getId();
				lat1 = Float.valueOf(lat);
				lng1 = Float.valueOf(lon);
				lat2 = Float.valueOf(park.getLatitude().toString());
				lng2 = Float.valueOf(park.getLongitude().toString());
				distance = distFrom(lat1, lng1, lat2, lng2);
				
				if(distance<1000){
				DistanceObject newDistance = new DistanceObject(distanceID, String.valueOf(distance), parkID, apartmentID);
				
				parkList.add(newDistance);
				}
				
			}
			
			
			
			
			
			
			
			
			
			
			
			apartmentsEdited.add(
					new ApartmentEdited(
							price,
							size,
							lon,
							lat,
							link,
							pricePerSqrt,
							belowAverage,
							parkList,
							schoolList,
							id
							)
					);
				
				
			
			
			
			
			
			
			
		}
		
		
		
		
		
		return apartmentsEdited;
		
		
		
	}
	
	public ArrayList<SchoolEdited> getSchoolsEdited(){
		String name, address, lat, lon;

		this.schoolsEdited = new ArrayList<SchoolEdited>();
		for(int i = 0; i<this.schools.length;i++){
			
			name=schools[i].getLabel().getValue();	
			
			if(schools[i].getAlabel()==null)continue;
			else address = schools[i].getAlabel().getValue();
			lat = schools[i].getLat().getValue();
			lon = schools[i].getLong().getValue();
			
			if(name!=null && address!=null && lat!=null && lon!=null){
				this.schoolsEdited.add(new SchoolEdited("School"+i,name, lat, lon, address));
			
			}
			else continue;
			
			
		
			
			
		}
		System.out.println(this.schoolsEdited.size()+"newSchool");
		return this.schoolsEdited;
	}
	
	public ArrayList<ParkEdited> getParksEdited(){
		for(int i = 0; i<this.parks.length;i++){
			
			this.parksEdited.add(new ParkEdited(
					"Park"+i,
					parks[i].getName(),
					parks[i].getGeometry().getLocation().getLat(),
					parks[i].getGeometry().getLocation().getLng()
					)
			);
			
			
		}
		System.out.println("Anzahl ParksEdited "+this.parksEdited.size());
		for(int i = 0 ; i<this.parksEdited.size();i++){
			System.out.println("ParkNummer:"+i);
			System.out.println("ParkID:"+parksEdited.get(i).getId());
		}
		return this.parksEdited;
	}
	
	
	
	
	
	

	private float distFrom(float lat1, float lng1, float lat2, float lng2) {
	    double earthRadius = 6371000; //meters
	    double dLat = Math.toRadians(lat2-lat1);
	    double dLng = Math.toRadians(lng2-lng1);
	    double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	               Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
	               Math.sin(dLng/2) * Math.sin(dLng/2);
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    float dist = (float) (earthRadius * c);
	    return dist;
	    }
	
	
	
	
	
	
}
