
package tom.delle.semanticweb.schools;


import com.google.gson.annotations.SerializedName;

public class School{
   	private Alabel alabel;
   	private Label label;
   	@SerializedName("lat")
   	private Lat latitude;
   	@SerializedName("long")
   	private Long longitude;
   	private School school;

 	public Alabel getAlabel(){
		if(this.alabel==null)return null;
		else
 		return this.alabel;
	}
	public void setAlabel(Alabel alabel){
		this.alabel = alabel;
	}
 	public Label getLabel(){
		if(this.label==null)return null;
		else
 		return this.label;
	}
	public void setLabel(Label label){
		this.label = label;
	}
 	public Lat getLat(){
 		if(this.latitude==null)return null;
		return this.latitude;
	}
	public void setLat(Lat lat){
		this.latitude = lat;
	}
 	public Long getLong(){
 		if(this.longitude==null)return null;
		return this.longitude;
	}
	public void setLong(Long longitude){
		this.longitude = longitude;
	}
 	public School getSchool(){
 		if(this.school==null)return null;
		return this.school;
	}
	public void setSchool(School school){
		this.school = school;
	}
}
