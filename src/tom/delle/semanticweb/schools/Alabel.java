
package tom.delle.semanticweb.schools;


public class Alabel{
   	private String type;
   	private String value;

 	public String getType(){
		return this.type;
	}
	public void setType(String type){
		this.type = type;
	}
 	public String getValue(){
 		if(this.value==null)return null;
 		else
		return this.value;
	}
	public void setValue(String value){
		this.value = value;
	}
}
